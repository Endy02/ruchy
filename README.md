# Ruchy
-------------------------
### Welcome to Rushy App

>PHP 7.4 | mariadb | Javascript | scss

#### Resume
Ruchy allow you to :

- Create an account and log into it
    - Edit your profile picture
    - Edit or update your personnal informations
- Reset your password
- Send an email if you forgot your password

### First Step

Clone this repository with this command
``
git clone https://gitlab.com/Endy02/ruchy.git
``

### Setup

1. Run a simple docker command
`` docker-compose up -d ``

2. Enter in the ruchy project
``
cd ruchy
``
3. run the npm commande
`` npm install ``

4. run webpack
   `` npm run webpack ``
   
5. Just go on this url to see the home page
[Local host Link](https://localhost:80)

### Other informations

#### phpmyadmin

after you start the docker containers you will need an access to phpmyadmin and manage your database as well

[phpmyadmin link](https://localhost:8183)

> If you want to manage your database you will need this informations below <br>
>> **username** : default
>> **Password** : defaultpassword
> 
><br>
> You can find and edit it in the docker-compose file


