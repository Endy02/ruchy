<?php

namespace Ruchy\Models;

use Ruchy\Core\Model;
use Ruchy\Core\ModelInterface;
use DateTime;

class User extends Model implements ModelInterface
{
    private $id;
    private $firstname;
    private $lastname;
    private $username;
    private $email;
    private $password;
    private $ppUrl;
    private $token;
    private $createdAt;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->createdAt = $date;
    }

    /**
     * @return array
     */
    public function initRelation(): array
    {
        return [

        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id=$id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = htmlspecialchars(ucwords(strtolower(trim($firstname))));

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = htmlspecialchars(ucwords(strtolower(trim($lastname))));
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = htmlspecialchars(strtolower(trim($username)));
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = htmlspecialchars(strtolower(trim($email)));
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPpUrl(): ?string
    {
        return $this->ppUrl;
    }

    /**
     * @param mixed $ppUrl
     */
    public function setPpUrl(string $ppUrl): self
    {
        $this->ppUrl = $ppUrl;
        return $this;
    }

    public function getToken(): ?string{
        return $this->token;
    }

    public function setToken(?string $token): self{
        $this->token = $token;
        return $this;
    }
}














