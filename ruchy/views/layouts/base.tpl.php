<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ruchy</title>
    <script src="<?= $this->assets('js','app.js') ?>"></script>
</head>
<body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/views/modules/navbar.view.php"); ?>
    <div class="container-fluid">
        <?php include("views/".$this->view.".view.php"); ?>
    </div>
    <?php include($_SERVER['DOCUMENT_ROOT']."/views/modules/footer.view.php"); ?>
</body>
</html>