<form
    method="<?= $form->getConfig()['method'] ?>"
    action="<?= $form->getAction() ?>"
    name="<?= $form->getName() ?>"
    <?php foreach($form->getConfig()['attr'] as $attr => $value)
    {
        echo "$attr = '$value' ";
    }
    ?>>
    <?php
    if(!$form->isValid())
    {
        foreach($form->getErrors() as $key => $errorsPerField)
        {
            foreach($errorsPerField as $error)
            {
                echo "Erreur : $error <br>";

            }

        }
    }

    echo "<br><br>";
    ?>


    <?php foreach ($form->getElements() as $key => $field):?>
        <div class="form-item">
                <?php if (isset($field->getOptions()["label"])){ ?>
                    <label
                        <?php
                        if(isset($field->getOptions()['attr_label'])) {
                            foreach($field->getOptions()['attr_label'] as $attr => $value)
                            {
                                echo "$attr = '$value' ";
                            }
                        }

                        ?>
                    >
                        <?= $field->getOptions()["label"] ?></label>
                        <hr class="input-separator">
                <?php } ?>
                    <?php switch ($field->getType()) {
                        case "checkbox":?>
                            <input
                                    type="<?= $field->getType() ?>"
                                    name="<?= $form->getName().'_'.$field->getName() ?>"
                                <?php
                                if(isset($field->getOptions()['attr'])) {
                                    foreach($field->getOptions()['attr'] as $attr => $value)
                                    {
                                        echo "$attr = '$value' ";
                                    }
                                }
                                ?>
                                <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
                        <?php
                        break;
                        case "select":?>
                            <select name="<?= $form->getName().'_'.$field->getName() ?>"
                            <?php
                            if(isset($field->getOptions()['attr'])) {
                                foreach($field->getOptions()['attr'] as $attr => $value)
                                {
                                    echo "$attr = '$value' ";
                                }
                            }
                            ?>
                            <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
                                <?php
                                foreach($field->getOptions()['options'] as $attr => $value)
                                {
                                ?>
                                    <option value="<?= $attr ?>" ><?= $value ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        <?php
                        break;
                        case "textarea":?>
                            <textarea name="<?= $form->getName().'_'.$field->getName() ?>"
                                <?php
                                if(isset($field->getOptions()['attr'])) {
                                    foreach($field->getOptions()['attr'] as $attr => $value)
                                    {
                                        echo "$attr = '$value' ";
                                    }
                                }
                                ?>
                                <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
                            </textarea>
                            <?php
                            break;
                        case "button":?>
                        <div class="form-btn">
                            <input
                                    value="<?= (isset($field->getOptions()['value'])) ? $field->getOptions()['value']:'' ?>"
                                    type="submit"
                                    name="<?= $form->getName().'_'.$field->getName() ?>"
                                <?php
                                if(isset($field->getOptions()['attr'])) {
                                    foreach($field->getOptions()['attr'] as $attr => $value)
                                    {
                                        echo "$attr = '$value' ";
                                    }
                                }
                                ?>
                            >
                        </div>
                        <?php
                            break;
                        default:
                        ?>
                            <input
                                value="<?= (isset($field->getOptions()['value'])) ? $field->getOptions()['value']:'' ?>"
                                type="<?= $field->getType() ?>"
                                name="<?= $form->getName().'_'.$field->getName() ?>"
                            <?php
                            if(isset($field->getOptions()['attr'])) {
                                foreach($field->getOptions()['attr'] as $attr => $value)
                                {
                                    echo "$attr = '$value' ";
                                }
                            }
                            ?>
                            <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
                        <?php }?>
        </div>
    <?php endforeach;?>
</form>
