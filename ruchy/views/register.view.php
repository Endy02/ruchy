<div class="login-area">
    <div class="login-form">
        <!--<form action="" class="form-wrapper">
            <div class="form-item">
                <label for="username" class="input-label">Username</label>
                <hr class="input-separator">
                <input type="text" class="input-round" id="username">
            </div>
            <div class="form-item">
                <label for="email" class="input-label">Email</label>
                <hr class="input-separator">
                <input type="text" class="input-round" id="email">
            </div>
            <div class="form-item">
                <label for="password" class="input-label">Password</label>
                <hr class="input-separator">
                <input type="password" class="input-round" id="password">
            </div>
            <div class="form-item">
                <label for="confirm_password" class="input-label">Confirm Password</label>
                <hr class="input-separator">
                <input type="password" class="input-round" id="confirm_password">
            </div>
            <div class="form-item">
                <div class="form-btn">
                    <button type="submit" class="btn-submit">REGISTER</button>
                </div>
            </div>
        </form>-->
        <?php $this->formView('registerForm') ?>
    </div>
    <div class="login-links">
        <p>Already have an account ? </p>
        <a href="/login">LOGIN</a>
    </div>
</div>
