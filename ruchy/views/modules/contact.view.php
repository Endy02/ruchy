<div class="main-container bg-red" id="contact-us">
    <div class="main-wrapper">
        <div class="main-content">
            <h1>You want to contact me ?</h1>
            <p>Just enter your email and send me your feedback about the site or what you want in the form on the right side.</p>
        </div>
        <div class="contact-form">
            <?php $this->formView('contactForm') ?>
        </div>
    </div>
</div>