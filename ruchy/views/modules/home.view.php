<div class="main-container bg-red" id="home">
    <div class="main-wrapper">
        <div class="main-content">
            <h1>Why do you need an account on this site ?</h1>
            <p>It's because i need you to test all of the functionality of this site to have a point of view to my work and know what's my passion</p>
            <button class="main-btn"><a href="/register">REGISTER</a></button>
        </div>
        <div class="main-image-container">
            <img src="<?= $this->assets('img','home-img.svg') ?>" alt="" id="main-img">
        </div>
    </div>
</div>