<div class="main-container bg-white">
    <div class="main-wrapper">
        <div class="profil-content">
            <div class="profil-pics">
                <div class="big-circle">
                    <img src="<?= $this->assets('img', $user->getPpUrl()) ?>" alt="">
                </div>
                <?php $this->formView('uploadPics') ?>
                <div class="username"><?= $user->getUsername() ?></div>
                <div class="profil-sets">
                    <h1 class="set-title">Account creation</h1>
                    <p class="set-content"><?= $user->getCreatedAt() ?></p>
                </div>
            </div>
            <hr class="profil-separator">
            <div class="profil-infos">
                <div class="profil-form">
                    <div class="general-infos">
                        <h1 class="form-title">General Informations</h1>
                        <?php $this->formView('editUserForm') ?>
                        <a href="/new-password/<?= $_SESSION['user_id'] ?>/<?= $_SESSION['token']?>">New Password</a>
                    </div>
                    <div class="other-infos">
                        <div class="errors">
                            <?php if(isset($_SESSION['errors'])){foreach ($_SESSION['errors'] as $error => $value){ ?>
                                <h6 class="success-title"><?= $error ?></h6>
                                <p class="error-content"><?= $value ?></p>
                            <?php }}?>
                            <?php if(isset($_SESSION['success'])){foreach ($_SESSION['errors'] as $success => $value){ ?>
                                <h6 class="success-title"><?= $success ?></h6>
                                <p class="success-content"><?= $value ?></p>
                            <?php }}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>