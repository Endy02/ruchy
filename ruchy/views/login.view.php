<div class="login-area">
    <div class="login-form">
        <?php $this->formView('loginForm') ?>
    </div>
    <div class="login-links">
        <p>Don't have an account ? Okay, just</p>
        <a href="/register">REGISTER</a>
    </div>
    <div class="login-links">
        <a href="/forgot-password">Forgot Password</a>
    </div>
</div>
