<?php

namespace Ruchy\Forms\User;

use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Constraints\Length;
use Ruchy\Models\User;
use Ruchy\Core\Form;

class ForgotPwdType extends Form
{
    public function buildForm(FormBuilder $builder)
    {
        $this->setBuilder(
            $builder
                ->add('email', 'email', [
                    'required' => true,
                    'label' => 'Email',
                    'attr_label' =>[
                        'for' => 'email',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-email'
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SEND',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );

    }

    public function configureOptions(): void
    {
        $this
            ->setName('forgotPwdType')
            ->addConfig('attr', [
                "id"=>"formForgotPwd",
                "class"=>"form-wrapper",
            ]);
    }

}