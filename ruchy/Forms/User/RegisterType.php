<?php

namespace Ruchy\Forms\User;

use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Models\User;
use Ruchy\Core\Form;

class RegisterType extends Form {

    public function buildForm(FormBuilder $builder)
    {
        $this->setBuilder(
            $builder
                ->add('firstname', 'text', [
                    'required' => true,
                    'label' => 'Firstname',
                    'attr_label' =>[
                        'for' => 'firstname',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-name',
                        'max-length' => 10,
                        'min-length' => 4
                    ]
                ])
                ->add('lastname', 'text', [
                    'required' => true,
                    'label' => 'Lastname',
                    'attr_label' =>[
                        'for' => 'lastname',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-name',
                        'max-length' => 20,
                        'min-length' => 4
                    ]
                ])
                ->add('username', 'text', [
                    'required' => true,
                    'label' => 'Username',
                    'attr_label' =>[
                        'for' => 'username',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-usernamme',
                        'max-length' => 10,
                        'min-length' => 4
                    ]
                ])
                ->add('email', 'email', [
                    'required' => true,
                    'label' => 'Email',
                    'attr_label' =>[
                        'for' => 'email',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-email',
                    ]
                ])
                ->add('password', 'password', [
                    'required' => true,
                    'label' => 'Password',
                    'attr_label' =>[
                        'for' => 'password',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-password',
                        'max-length' => 100,
                        'min-length' => 6

                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SAVE',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('class', User::class)
            ->setName('UserType')
            ->addConfig('attr', [
                "id"=>"formUser",
                "class"=>"form-wrapper"
            ]);
    }

}
