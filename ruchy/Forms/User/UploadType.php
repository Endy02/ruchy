<?php


namespace Ruchy\Forms\User;


use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Form;
use Ruchy\Models\User;

class UploadType extends Form
{
    public function buildForm(FormBuilder $builder){
        $this->setBuilder(
            $builder
                ->add('picture', 'file', [
                    'required' => true,
                    'label' => 'Picture',
                    'attr_label' =>[
                        'for' => 'picture',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'name' => 'userfile',
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-file',
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SAVE',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );
    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('class', User::class)
            ->setName('UploadType')
            ->addConfig('attr', [
                "id"=>"formUpload",
                "class"=>"form-wrapper",
                "enctype" => "multipart/form-data"
            ]);
    }
}