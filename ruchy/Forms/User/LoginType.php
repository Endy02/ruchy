<?php

namespace Ruchy\Forms\User;

use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Form;
use Ruchy\Models\User;

class LoginType extends Form {

    public function buildForm(FormBuilder $builder){
        $this->setBuilder(
            $builder
                ->add('username', 'text', [
                    'required' => true,
                    'label' => 'Username',
                    'attr_label' =>[
                        'for' => 'username',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-usernamme',
                    ]
                ])
                ->add('password', 'password', [
                    'required' => true,
                    'label' => 'Password',
                    'attr_label' =>[
                        'for' => 'password',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-password',
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'LOGIN',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );
    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('class', User::class)
            ->setName('loginType')
            ->addConfig('attr', [
                "id"=>"formLogin",
                "class"=>"form-wrapper",
            ]);
    }
}
