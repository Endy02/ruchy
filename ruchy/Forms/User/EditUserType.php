<?php


namespace Ruchy\Forms\User;


use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Form;
use Ruchy\Models\User;

class EditUserType extends Form
{
    public function buildForm(FormBuilder $builder)
    {
        $this->setBuilder(
            $builder
                ->add('firstname', 'text', [
                    'required' => true,
                    'label' => 'Firstname',
                    'attr_label' =>[
                        'for' => 'firstname',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-name',
                        'max-length' => 10,
                        'min-length' => 4
                    ]
                ])
                ->add('lastname', 'text', [
                    'required' => true,
                    'label' => 'Lastname',
                    'attr_label' =>[
                        'for' => 'lastname',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-name',
                        'max-length' => 20,
                        'min-length' => 4
                    ]
                ])
                ->add('username', 'text', [
                    'required' => true,
                    'label' => 'Username',
                    'attr_label' =>[
                        'for' => 'username',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-usernamme',
                        'max-length' => 10,
                        'min-length' => 4
                    ]
                ])
                ->add('email', 'email', [
                    'required' => true,
                    'label' => 'Email',
                    'attr_label' =>[
                        'for' => 'email',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-email',
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SAVE',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('class', User::class)
            ->setName('EditUserType')
            ->addConfig('attr', [
                "id"=>"formEditUser",
                "class"=>"form-wrapper"
            ]);
    }
}