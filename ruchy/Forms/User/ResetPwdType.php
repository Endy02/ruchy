<?php

namespace Ruchy\Forms\User;

use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Models\User;
use Ruchy\Core\Form;

class ResetPwdType extends Form
{
    public function buildForm(FormBuilder $builder)
    {
        $this->setBuilder(
            $builder
                ->add('password', 'password', [
                    'required' => true,
                    'label' => 'New password',
                    'attr_label' =>[
                        'for' => 'password',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-password',
                    ]
                ])
                ->add('confirmPassword', 'password', [
                    'required' => true,
                    'label' => 'Confirm Password',
                    'attr_label' =>[
                        'for' => 'confirmPassword',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ],
                    'constraints' => [
                        'type' => 'input-password',
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SAVE',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('class', User::class)
            ->setName('resetPwdType')
            ->addConfig('attr', [
                "id"=>"formResetPwd",
                "class"=>"form-wrapper",
            ]);
    }

}