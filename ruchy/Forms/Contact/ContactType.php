<?php


namespace Ruchy\Forms\Contact;


use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Form;
use Ruchy\Models\User;

class ContactType extends Form
{
    public function buildForm(FormBuilder $builder){
        $this->setBuilder(
            $builder
                ->add('Email', 'text', [
                    'required' => true,
                    'label' => 'Email',
                    'attr_label' =>[
                        'for' => 'username',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-round'
                    ]
                ])
                ->add('feedback', 'textarea', [
                    'required' => true,
                    'label' => 'Feedback',
                    'attr_label' =>[
                        'for' => 'feedback',
                        'class' => 'input-label'
                    ],
                    'attr' => [
                        'class' => 'input-area',
                        'id'=>'feed',
                        'rows' => '5',
                        'cols' => '15'
                    ]
                ])
                ->add('submit', 'button', [
                    'value' => 'SEND',
                    'attr' => [
                        'class'=>"btn-submit"
                    ]
                ])
        );
    }

    public function configureOptions(): void
    {
        $this
            ->setName('ContactType')
            ->addConfig('attr', [
                "id"=>"ContactForm",
                "class"=>"form-wrapper",
            ]);
    }

}