const path = require('path');

module.exports = {
    mode: "development",
    watch: true,
    entry: "./assets/js/main.js",
    output: {
        filename: "app.js",
        path: path.resolve(__dirname, "public/js")
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader?sourceMap",
                ],
            },
        ],
    },
};
