<?php
namespace Ruchy;

class Autoloader {

    static function register(){
        spl_autoload_register(array(__CLASS__,'autoload'));
    }

    static function autoload($class){

        if(strpos($class,__NAMESPACE__.'\\') === 0){
            $class = str_replace(__NAMESPACE__.'\\','',$class);
            $class = str_replace('\\','/',$class);
            
            if(strpos($class,"Controllers")){
                $class = str_replace('\\','/',$class);
                $class = strstr($class, "Controllers");
            }
            require $class . '.php';
        }
    }
}

