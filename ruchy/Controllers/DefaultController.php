<?php

namespace Ruchy\Controllers;

use Ruchy\Core\Controller;
use Ruchy\Core\Mailer\Mail;
use Ruchy\Forms\Contact\ContactType;

class DefaultController extends Controller{

    public function __construct(){
        parent::__construct();
    }


    /**
     * Route /
     */
    public function default(){

        $form = $this->createForm(ContactType::class);
        $form->handle();
        if ($form->isSubmit() && $form->isValid()){
            $mail = new Mail('endy.alexis@outlook.com',$_POST['ContactType_feedback'],'Fujyn','Contact Mail');
            if ($mail->sendMail()){
                header('Location: /');
            }
        }

        $this->render('base','site',[
            'contactForm' => $form,
        ]);
    }

}