<?php

namespace Ruchy\Controllers;

use Cassandra\Date;
use DateTime;
use Ruchy\Core\Connection\PDOConnection;
use Ruchy\Core\Controller;
use Ruchy\Core\Mailer\Mail;
use Ruchy\Forms\User\EditUserType;
use Ruchy\Forms\User\ForgotPwdType;
use Ruchy\Forms\User\LoginType;
use Ruchy\Forms\User\RegisterType;
use Ruchy\Forms\User\ResetPwdType;
use Ruchy\Forms\User\UploadType;
use Ruchy\Managers\UserManager;
use Ruchy\Models\User;

class UserController extends Controller
{
    protected UserManager $um;
    protected PDOConnection $pdo;

    public function __construct()
    {
        parent::__construct();
        $this->um = new UserManager();
        $this->pdo = new PDOConnection();

    }
    
    /**
     * ROUTE /login
     */
    public function login()
    {
        $user = new User();
        $form = $this->createForm(LoginType::class, $user);
        $form->handle();

        if ($this->checkSession()){
            header('Location: /');
        }

        if ($form->isSubmit() && $form->isValid()){
            $user = $this->um->findOneBy(['username'=> $_POST['loginType_username']]);
            if ($user){
                if (password_verify($_POST['loginType_password'],$user->getPassword())){
                    $this->userSession($user);
                    header('Location: /');
                }else{
                    $_SESSION['errors']['password'] = "Password doesn't match";
                }
            }else{
                $_SESSION['errors']['username'] = "Username does not exist";
            }
        }

        $this->render('login','login', [
            'loginForm' => $form
        ]);
    }
    /**
     * ROUTE /register
     */
    public function register()
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handle();

        if ($this->checkSession()){
            header('Location: /');
        }

        if ($form->isSubmit() && $form->isValid()){
            if ($this->checkEmail($_POST['UserType_email']) && $this->checkUsername($_POST['UserType_username']) ){
                if ($this->createUser($_POST)){
                    $createUser = $this->um->findOneBy(['username' => $_POST['UserType_username']]);
                    $this->userSession($createUser);
                    header('Location: /');
                }
            }
        }

        $this->render('login','register', [
            'registerForm' => $form,
        ]);
    }

    /**
     * ROUTE /confirm/:id/:token
     * @param $id
     * @param $token
     */
    public function confirmAccount($id,$token){
        $user = $this->um->findOneBy(['id'=>$id]);
        var_dump($token);
        var_dump($user->getToken());
        if(strcmp($user->getToken(),$token) != 0 ){
            $sql = "UPDATE user SET confirmAt=:confirmAt WHERE id={$id}";
            $date = new DateTime();
            $date = $date->format('Y-m-d H:i:s');
            $this->pdo->query($sql,[
                ':confirmAt' => $date,
            ]);
            header('Location: /');
        }else{
            $_SESSION['errors']['account'] = "Your account can't be confirmed";
        }
    }

    /**
     * ROUTE /forgot-password
     */
    public function forgotPassword(){
        $form = $this->createForm(ForgotPwdType::class);
        $form->handle();

        if ($form->isSubmit() && $form->isValid()){
            $array = $this->cleanPost($_POST);
            $user = $this->um->findOneBy(['email'=> $array['email']]);
            if (isset($user)){
                $mail = new Mail($user->getEmail(),"Hey {$user->getUsername()} \n\n To reset your password please click on link below : \n\n http://localhost/new-password/{$user->getId()}/{$user->getToken()}",$user->getUsername(),'Reset Password');
                if ($mail->sendMail()){
                    header('Location: /login');
                }
            }
        }

        $this->render('login','forgot_password',[
            'forgotPassForm' => $form
        ]);
    }

    /**
     * ROUTE /profile/:id
     * @param $id
     */
    public function profile($id){
        $user = $this->um->findOneBy(['id'=>$id]);
        $formPics = $this->createForm(UploadType::class,$user);
        $formEdit = $this->createForm(EditUserType::class,$user);
        $formEdit->handle();
        $formPics->handle();

        if ($id != $_SESSION['user_id']){
            header('Location: /');
        }

        if ($formEdit->isSubmit() && $formEdit->isValid()){
            $update = $this->checkBeforeUpdate($_POST,$id);
            if($update){
                $this->updateUser($update,$id);
                $this->userSession($user);
            }
        }

        if ($formPics->isSubmit() && $formPics->isValid()){
            if($this->checkFile($_FILES,$id)){
                $this->userSession($user);
            }
        }

        if (!$this->checkSession()){
            header('Location: /login');
        }

        $this->render('base','modules/profil',[
            'uploadPics' => $formPics,
            'editUserForm' => $formEdit,
            'user' => $user
        ]);
    }

    /**
     * To logout session
     * ROUTE: /logout
     */
    public function logout(){
        session_destroy();
        header('Location: /');
    }

    /**
     * TEST Password Reset
     * ROUTE: /new-password/:id
     * @param $id
     * @param $token
     */
    public function newPassword($id,$token){
        $user = $this->um->findOneBy(['id'=>$id]);
        $form = $this->createForm(ResetPwdType::class,$user);
        $form->handle();

        if (isset($_SESSION['user_id']) && $id != $_SESSION['user_id']){
            header('Location: /');
        }

        if ($form->isSubmit() && $form->isValid()){
            if ($this->checkPass($_POST)){
                $array = [
                    'password' => password_hash($_POST['resetPwdType_password'],PASSWORD_BCRYPT)
                ];
                if($this->updateUser($array,$id)){
                    header('Location: /login');
                }
            }
        }
        $this->render('login','new_password',[
            'newPassForm' => $form
        ]);
    }

    /**
     * To Check Confirm Password
     * @param $post
     * @return bool
     */
    private function checkPass($post){
        $array = $this->cleanPost($post);
        if(strcmp($array['password'],$array['confirmPassword']) != 0){
            return false;
        }
        return true;
    }

    /**
     * To save new user in database
     * @param $post
     * @return bool
     */
    private function createUser($post): bool{
        $cleanArray = $this->cleanPost($post);
        $cleanArray['token'] = $this->createToken(60);
        $cleanArray['ppUrl'] = "default.jpeg";
        $sql = "INSERT INTO user (".implode(',',array_keys($cleanArray)).") VALUES (:".implode(',:',array_keys($cleanArray)).");";
        $array = array(
            ':firstname' => $cleanArray['firstname'],
            ':lastname' => $cleanArray['lastname'],
            ':username' => $cleanArray['username'],
            ':email' => $cleanArray['email'],
            ':password' => password_hash($cleanArray['password'], PASSWORD_BCRYPT),
            ':token' => $cleanArray['token'].'?',
            ':ppUrl' => $cleanArray['ppUrl']
        );
        if($this->pdo->query($sql,$array)){
            $mail = new Mail($cleanArray['email'],"Hey {$cleanArray['username']} \n\n To confirm your account please click on link below : \n\n http://localhost/confirm/{$this->pdo->getLastId()}/{$cleanArray['token']}",$cleanArray['username'],'Confirm Account');
            if ($mail->sendMail()){
                return true;
            }
        }
        return false;
    }

    /**
     * To Check Value Before Update User
     * @param $post
     * @param $id
     * @return array|null
     */
    private function checkBeforeUpdate($post,$id): ?array{
        $cleanArray = $this->cleanPost($post);
        $user = $this->um->findOneBy(['id'=>$id]);
        $array = [];
        foreach ($cleanArray as $name => $value){
            switch ($name){
                case 'firstname':
                    if ($user->getFirstname() != $value){
                        $array[$name] = $value;
                    }
                    break;
                case 'lastname':
                    if ($user->getLastname() != $value){

                        $array[$name] = $value;
                    }
                    break;
                case 'username':
                    if ($user->getUsername() != $value){
                        $array[$name] = $value;
                    }
                    break;
                case 'email':
                    if ($user->getEmail() != $value){
                        $array[$name] = $value;
                    }
                    break;
                case 'password':
                    if ($user->getPassword() != $value){
                        $array[$name] = $value;
                    }
                    break;
            }
        }
        if(!isset($array))
        {
            $_SESSION['errors']['update User'] = "Nothing to update";
            return null;
        }
        return $array;
    }

    /**
     * TO Update a User
     * @param $post
     * @param $id
     * @return bool
     */
    private function updateUser($post,$id): bool{
        $reqArray = [];
        $params = [];

        foreach($post as $name => $value){
            $reqArray[$name] = "{$name}=:{$name}";
            $params[$name] = $value;
        }
        $params = array_combine(
            array_map(function($k){ return ':'.$k; }, array_keys($params)),
            $params
        );
        $sql = "UPDATE user SET ".implode(',',$reqArray)." WHERE id=".$id.";";

        if($this->pdo->query($sql,$params)){
            $_SESSION['success']['updateUser'] = "Update Done !";
            return true;
        }
        return false;
    }

    /**
     * To Create a new user
     * @param $email
     * @return bool
     */
    private function checkEmail($email): bool{
        $user = $this->um->findOneBy(['email' => $email]);
        if ($user){
            $_SESSION['errors']['email'] = "This email is already used";
            return false;
        }
        return true;
    }

    /**
     * To check if username exist
     * @param $username
     * @return bool
     */
    private function checkUsername($username): bool{
        $user = $this->um->findOneBy(['username' => $username]);
        if ($user){
            $_SESSION['errors']['username'] = "This username is already used";
            return false;
        }
        return true;
    }

    /**
     * To Check and Save The file passed
     * @param $file
     * @param $id
     * @return bool
     */
    private function checkFile($file,$id): bool{
        $validExt = ['jpg','png','gif','jpeg'];
        $extension = explode('.',$file['UploadType_picture']['name'])[1];
        if (in_array($extension,$validExt)){
            $imgLocation = $_SERVER['DOCUMENT_ROOT'].'/public/img/'.$id.'.'.$extension;
            $result = move_uploaded_file($file['UploadType_picture']['tmp_name'], $imgLocation);
            if($result){
                $array = [
                    'ppUrl' => $id.'.'.$extension
                ];
                if($this->updateUser($array,$id)){
                    return true;
                }

            }
        }
        $_SESSION['errors']['File'] = "The file doesn't match with the authorized types [jpg,png,jpeg,gif]";
        return false;
    }

    /**
     * To clean Post array keys (example = UserType_username => username)
     * @param $array
     * @return array
     */
    private function cleanPost($array): array{
        $cleanArray = [];
        array_pop($array);

        foreach ($array as $value => $key){
            $cleanValue = explode('_',$value);
            $cleanArray[$cleanValue[1]] = $key;
        }
        return $cleanArray;
    }
}