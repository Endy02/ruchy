/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/components/progress.js":
/*!******************************************!*\
  !*** ./assets/js/components/progress.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("window.addEventListener('load', () => {\n    const progress = document.querySelector('.progress-wrapper')\n    var elem = document.getElementById(\"login-bar\");   \n    var width = 1;\n    var id = setInterval(frame, 10);\n    function frame() {\n        if (width >= 100) {\n            progress.classList.add('hidden');\n        } else {\n            width++; \n            elem.style.width = width + '%'; \n        }\n    }\n})\n\n//# sourceURL=webpack:///./assets/js/components/progress.js?");

/***/ }),

/***/ "./assets/js/main.js":
/*!***************************!*\
  !*** ./assets/js/main.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/main.scss */ \"./assets/scss/main.scss\");\n/* harmony import */ var _scss_main_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_main_scss__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _components_progress__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/progress */ \"./assets/js/components/progress.js\");\n/* harmony import */ var _components_progress__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_progress__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\n//# sourceURL=webpack:///./assets/js/main.js?");

/***/ }),

/***/ "./assets/scss/main.scss":
/*!*******************************!*\
  !*** ./assets/scss/main.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!../../node_modules/sass-loader/dist/cjs.js?sourceMap!./main.scss */ \"./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js?sourceMap!./assets/scss/main.scss\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./assets/scss/main.scss?");

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js?sourceMap!./assets/scss/main.scss":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js?sourceMap!./assets/scss/main.scss ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Imports\nvar ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\nexports = ___CSS_LOADER_API_IMPORT___(false);\n// Module\nexports.push([module.i, \".big-circle img {\\n  width: 25vh;\\n  height: 25vh;\\n  border-radius: 50%;\\n  object-fit: cover; }\\n\\n.x-small-circle {\\n  width: 6vh;\\n  height: 6vh;\\n  border-radius: 50%;\\n  background-color: #FBFBFB; }\\n\\n.bg-red {\\n  background-color: #BF0426;\\n  color: #FBFBFB; }\\n\\n.bg-white {\\n  background-color: #FBFBFB;\\n  color: #464646; }\\n\\n.bg-blue {\\n  background-color: #038ABF;\\n  color: #FBFBFB; }\\n\\n.input-label {\\n  width: 30%; }\\n\\n.input-label-small {\\n  width: 30%;\\n  font-size: 15px;\\n  font-weight: lighter; }\\n\\n.input-separator {\\n  height: 5vh;\\n  width: .8vh;\\n  border: 0;\\n  border-radius: 10px;\\n  background-color: #038ABF; }\\n\\n.input-round {\\n  border: 0;\\n  border-radius: 50px;\\n  padding: 5px 15px 5px 15px; }\\n\\n.input-round-border {\\n  border: 1.5px solid #464646;\\n  border-radius: 50px;\\n  padding: 5px 15px 5px 15px; }\\n\\n.input-area {\\n  border: 0;\\n  border-radius: 10px;\\n  padding: 5px 15px 5px 15px; }\\n\\n.container-fluid {\\n  width: 100%; }\\n\\n.container-mid {\\n  width: 100%;\\n  display: flex;\\n  justify-content: center;\\n  align-items: center;\\n  flex-direction: column; }\\n\\n.form-wrapper {\\n  display: flex;\\n  flex-direction: column;\\n  align-items: center;\\n  justify-content: center; }\\n  .form-wrapper .form-item {\\n    display: flex;\\n    flex-direction: row;\\n    align-items: center;\\n    justify-content: space-between;\\n    width: 100%;\\n    margin-bottom: 20px;\\n    font-weight: 600; }\\n    .form-wrapper .form-item label {\\n      cursor: pointer; }\\n    .form-wrapper .form-item input {\\n      cursor: pointer; }\\n    .form-wrapper .form-item .form-btn {\\n      display: flex;\\n      align-items: center;\\n      justify-content: center;\\n      width: 100%; }\\n\\n.progress-wrapper {\\n  position: fixed;\\n  z-index: 99;\\n  top: 0;\\n  left: 0;\\n  width: 100%;\\n  height: 100%;\\n  display: flex;\\n  align-items: center;\\n  justify-content: center; }\\n\\n.progress-wrapper.hidden {\\n  animation: fadeOut 1s;\\n  animation-fill-mode: forwards; }\\n\\n@keyframes fadeOut {\\n  100% {\\n    opacity: 0;\\n    visibility: hidden; } }\\n\\n.progress {\\n  width: 30%;\\n  height: 5vh;\\n  border-radius: 50px; }\\n  .progress .progress-bar {\\n    height: 5vh;\\n    border-radius: 50px; }\\n\\n.btn-submit {\\n  border-radius: 50px;\\n  padding: 8px 50px;\\n  border: 0;\\n  background-color: #47C838;\\n  font-weight: 600;\\n  color: #FBFBFB;\\n  font-size: 18px;\\n  cursor: pointer; }\\n  .btn-submit:hover {\\n    background-color: #038ABF;\\n    transition: all .55s ease-out; }\\n\\n.navbar {\\n  background: #BF0426;\\n  display: flex;\\n  justify-content: center;\\n  align-items: center;\\n  font-size: 1rem;\\n  position: sticky;\\n  top: 0;\\n  z-index: 1;\\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16); }\\n  .navbar .navbar-wrapper {\\n    color: #FBFBFB;\\n    display: flex;\\n    flex-direction: row;\\n    align-items: center;\\n    justify-content: space-between;\\n    width: 100%;\\n    margin: 5px 10px; }\\n  .navbar .container {\\n    z-index: 1;\\n    width: 100%;\\n    margin: 5px 0;\\n    padding: 0 30px; }\\n  .navbar .navbar-logo {\\n    color: #FBFBFB;\\n    font-weight: 700;\\n    font-size: 2rem;\\n    justify-self: start;\\n    text-decoration: none;\\n    cursor: pointer;\\n    display: flex;\\n    align-items: center; }\\n    .navbar .navbar-logo .cls-2 {\\n      fill: #FBFBFB; }\\n      .navbar .navbar-logo .cls-2:hover {\\n        fill: #038ABF;\\n        transition: all .35s ease; }\\n  .navbar .navbar-items {\\n    padding: 0;\\n    margin: 0;\\n    list-style: none;\\n    display: flex;\\n    justify-content: center;\\n    align-items: center;\\n    height: 100%; }\\n  .navbar .nav-item {\\n    text-decoration: none;\\n    cursor: pointer;\\n    height: 90%; }\\n  .navbar .nav-links {\\n    color: #FBFBFB;\\n    display: flex;\\n    align-items: center;\\n    text-decoration: none;\\n    padding: 0 1rem;\\n    height: 100%;\\n    justify-content: center;\\n    font-weight: 600;\\n    font-size: 16px; }\\n    .navbar .nav-links:hover {\\n      color: #038ABF;\\n      transition: all 0.1s ease-out; }\\n  .navbar .navbar-tools {\\n    display: flex;\\n    justify-content: center;\\n    align-items: center;\\n    height: 100%;\\n    cursor: pointer; }\\n  .navbar .nav-tools-user {\\n    padding-right: 10px;\\n    display: flex;\\n    flex-direction: row;\\n    text-decoration: none;\\n    align-items: center;\\n    justify-content: space-between; }\\n    .navbar .nav-tools-user strong {\\n      padding-right: 10px;\\n      color: #FBFBFB; }\\n      .navbar .nav-tools-user strong:hover {\\n        color: #038ABF;\\n        transition: all 0.1s ease-in; }\\n\\nfooter {\\n  background: #038ABF;\\n  color: #FBFBFB;\\n  font-size: 0.8rem;\\n  font-weight: 600;\\n  bottom: 0;\\n  box-shadow: 0 -3px 6px 0 rgba(0, 0, 0, 0.16);\\n  width: 100%;\\n  display: flex;\\n  justify-content: center; }\\n\\n.footer-logo {\\n  width: auto;\\n  height: auto; }\\n\\n.footer-sign {\\n  display: flex;\\n  align-items: center; }\\n\\n.footer-sign-text {\\n  margin-right: 10px; }\\n\\n.footer-wrapper {\\n  width: 90%;\\n  display: flex;\\n  align-items: center;\\n  justify-content: space-between;\\n  padding: 8px 0; }\\n\\n.main h1 {\\n  margin: 0; }\\n\\n.main .main-container {\\n  display: flex;\\n  width: 100%;\\n  justify-content: center;\\n  align-items: center; }\\n  .main .main-container .main-wrapper {\\n    display: grid;\\n    grid-template-columns: 1fr 1fr;\\n    align-items: center;\\n    justify-self: center;\\n    width: 80%;\\n    margin: 20vh auto; }\\n\\n.main .main-content {\\n  justify-self: start;\\n  margin-left: 20px;\\n  color: white;\\n  width: 90%; }\\n  .main .main-content h1 {\\n    font-size: 2rem; }\\n  .main .main-content p {\\n    margin-top: 1rem;\\n    font-size: 1.2rem; }\\n  .main .main-content .main-btn {\\n    font-size: 1.5rem;\\n    font-weight: 600;\\n    background-color: #038ABF;\\n    padding: 8px 20px;\\n    border-radius: 5px;\\n    border: none;\\n    color: white;\\n    margin-top: 2rem;\\n    cursor: pointer;\\n    position: relative;\\n    transition: all .35s;\\n    outline: none; }\\n    .main .main-content .main-btn a {\\n      position: relative;\\n      z-index: 2;\\n      color: white;\\n      text-decoration: none; }\\n    .main .main-content .main-btn:after {\\n      position: absolute;\\n      content: '';\\n      top: 0;\\n      left: 0;\\n      width: 0;\\n      height: 100%;\\n      background-color: #47C838;\\n      border-radius: 5px;\\n      transition: all .35s; }\\n    .main .main-content .main-btn:hover {\\n      color: white;\\n      border-radius: 5px; }\\n    .main .main-content .main-btn:hover:after {\\n      width: 100%;\\n      border-radius: 5px; }\\n\\n.main .main-image-container {\\n  display: flex;\\n  justify-content: flex-end;\\n  align-items: center; }\\n  .main .main-image-container #main-img {\\n    height: 70%;\\n    width: 70%; }\\n\\n.about-wrapper {\\n  color: #464646; }\\n  .about-wrapper .about-title {\\n    display: flex;\\n    flex-direction: row;\\n    align-items: center; }\\n    .about-wrapper .about-title h1 {\\n      color: #BF0426;\\n      margin-right: 30px; }\\n\\n.about-container {\\n  display: flex;\\n  align-items: center;\\n  justify-content: center;\\n  width: 80%; }\\n  .about-container #about-img {\\n    width: 90%;\\n    height: 90%; }\\n\\n.login-wrapper {\\n  width: 100%;\\n  height: 100vh;\\n  display: flex;\\n  align-items: center;\\n  justify-content: center; }\\n  .login-wrapper .logo-mid {\\n    margin-bottom: 20px; }\\n  .login-wrapper .login-area {\\n    display: flex;\\n    flex-direction: column;\\n    width: 40%; }\\n    .login-wrapper .login-area .login-links {\\n      display: flex;\\n      flex-direction: row;\\n      align-items: center;\\n      justify-content: center; }\\n      .login-wrapper .login-area .login-links a {\\n        margin-left: 5px;\\n        text-decoration: none;\\n        color: #47C838;\\n        font-weight: 600; }\\n        .login-wrapper .login-area .login-links a:hover {\\n          color: #038ABF;\\n          transition: all .55s ease; }\\n  .login-wrapper .forgot-area {\\n    width: 30%; }\\n  .login-wrapper .new-area {\\n    width: 40%; }\\n\\n.contact-form {\\n  width: 80%;\\n  margin: 30px; }\\n\\n.profil-content {\\n  display: grid;\\n  grid-template-columns: 0.8fr 0.1fr 2.4fr;\\n  grid-template-rows: 1fr;\\n  gap: 0px 0px;\\n  grid-template-areas: \\\". . .\\\";\\n  height: 73vh;\\n  margin: 5.3vh 0; }\\n  .profil-content .profil-pics {\\n    display: flex;\\n    flex-direction: column;\\n    align-items: center;\\n    justify-content: space-around; }\\n    .profil-content .profil-pics .profil-sets {\\n      text-align: center;\\n      flex-direction: column; }\\n      .profil-content .profil-pics .profil-sets .set-title {\\n        font-size: 18px; }\\n  .profil-content .profil-infos {\\n    display: flex;\\n    flex-direction: column;\\n    align-items: center; }\\n    .profil-content .profil-infos .profil-banner {\\n      width: 90%;\\n      height: 15vh;\\n      background-color: #038ABF;\\n      border-radius: 20px; }\\n    .profil-content .profil-infos .profil-form {\\n      width: 90%;\\n      display: flex;\\n      flex-direction: row;\\n      justify-content: space-between; }\\n      .profil-content .profil-infos .profil-form .form-title {\\n        font-size: 20px; }\\n      .profil-content .profil-infos .profil-form .general-infos, .profil-content .profil-infos .profil-form .other-infos {\\n        width: 40%; }\\n\\n* {\\n  font-family: 'Roboto', sans-serif; }\\n\\nbody {\\n  margin: 0;\\n  padding: 0;\\n  box-sizing: border-box; }\\n\\nhtml {\\n  scroll-behavior: smooth; }\\n\", \"\"]);\n// Exports\nmodule.exports = exports;\n\n\n//# sourceURL=webpack:///./assets/scss/main.scss?./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js?sourceMap");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\nmodule.exports = function (useSourceMap) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item, useSourceMap);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \" {\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join('');\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery, dedupe) {\n    if (typeof modules === 'string') {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, '']];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var i = 0; i < this.length; i++) {\n        // eslint-disable-next-line prefer-destructuring\n        var id = this[i][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = [].concat(modules[_i]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        // eslint-disable-next-line no-continue\n        continue;\n      }\n\n      if (mediaQuery) {\n        if (!item[2]) {\n          item[2] = mediaQuery;\n        } else {\n          item[2] = \"\".concat(mediaQuery, \" and \").concat(item[2]);\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\nfunction cssWithMappingToString(item, useSourceMap) {\n  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring\n\n  var cssMapping = item[3];\n\n  if (!cssMapping) {\n    return content;\n  }\n\n  if (useSourceMap && typeof btoa === 'function') {\n    var sourceMapping = toComment(cssMapping);\n    var sourceURLs = cssMapping.sources.map(function (source) {\n      return \"/*# sourceURL=\".concat(cssMapping.sourceRoot || '').concat(source, \" */\");\n    });\n    return [content].concat(sourceURLs).concat([sourceMapping]).join('\\n');\n  }\n\n  return [content].join('\\n');\n} // Adapted from convert-source-map (MIT)\n\n\nfunction toComment(sourceMap) {\n  // eslint-disable-next-line no-undef\n  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));\n  var data = \"sourceMappingURL=data:application/json;charset=utf-8;base64,\".concat(base64);\n  return \"/*# \".concat(data, \" */\");\n}\n\n//# sourceURL=webpack:///./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nvar stylesInDom = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDom.length; i++) {\n    if (stylesInDom[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var index = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3]\n    };\n\n    if (index !== -1) {\n      stylesInDom[index].references++;\n      stylesInDom[index].updater(obj);\n    } else {\n      stylesInDom.push({\n        identifier: identifier,\n        updater: addStyle(obj, options),\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n  var attributes = options.attributes || {};\n\n  if (typeof attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : undefined;\n\n    if (nonce) {\n      attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(attributes).forEach(function (key) {\n    style.setAttribute(key, attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.media ? \"@media \".concat(obj.media, \" {\").concat(obj.css, \"}\") : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  } else {\n    style.removeAttribute('media');\n  }\n\n  if (sourceMap && typeof btoa !== 'undefined') {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    if (Object.prototype.toString.call(newList) !== '[object Array]') {\n      return;\n    }\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDom[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDom[_index].references === 0) {\n        stylesInDom[_index].updater();\n\n        stylesInDom.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack:///./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ })

/******/ });