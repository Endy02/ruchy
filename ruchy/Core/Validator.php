<?php 
namespace Ruchy\Core;

use Ruchy\Models\User;

class Validator{

	public static function checkForm($configForm, $data){
		$listOfErrors = [];

		//Vérifications

		//Vérifier le nb de input
		if( count($configForm["fields"]) == count($data) ) {

			foreach ($configForm["fields"] as $name => $config) {
				
				//Vérifie que l'on a bien les champs attendus
				//Vérifier les required
				if( !array_key_exists($name, $data) || 
					( $config["required"] && empty($data[$name]))
				){
					return ["Tentative de hack !!!"];
				}
				
				//Verify email
				if($config["type"]=="email"){
					
					if(self::checkEmail($data[$name])){
						//Vérifier l'unicité de l'email
                        $user = new User();
                        if ($user->getEmail()) {
                            return ['Cet email existe déja'];
                        }
					}else{
						$listOfErrors[]=$config["errorMsg"];
					}
				}

					

				//Vérifier le captcha
				if($_SESSION["captcha"] != '?????'){

				}

				//Vérifier le password
					//Vérifier les confirm

				//Vérifier le min
				//Vérifier le max
			}

		}else{
			return ["Tentative de hack !!!"];
		}

		return $listOfErrors;
	}

	public static function checkEmail($email){
		$email = trim($email);
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function checkPassword($password, $verificationPassword){

	}

}