<?php

namespace Ruchy\Core;

use Ruchy\Core\Helper;
use Ruchy\Core\Router;

class View
{
    private $template;
    private $url;
    private $view;
    private $um;
    private $data = [];

    public function __construct($view, $template="base")
    {
        $this->setTemplate($template);
        $this->setView($view);
    }

    public function setTemplate($t)
    {
        $this->template = strtolower(trim($t));
        if (!file_exists("views/layouts/".$this->template.".tpl.php")) {
            die("Le template n'existe pas");
        }
    }

    public function setView($v)
    {
        $this->view = strtolower(trim($v));

        if (!file_exists("views/".$this->view.".view.php")) {
            die("La vue n'existe pas");
        }
    }


    public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }

    // $this->addModal("carousel", $data);
    public function addModal($modal, $data)
    {
        if (!file_exists("views/modals/".$modal.".mod.php")) {
            die("Le modal n'existe pas!!!");
        }

        include "views/modals/".$modal.".mod.php";
    }


    //Methode permettant d'afficher un formulaire en lui passant le nom dans data (ici formProfile)
    //Et affichant la vue
    public function formView(string $formName, string $formTemplate = "base")
    {
        if (!file_exists("views/forms/".$formTemplate.".view.php")) {
            die("Le template de formulaire n'existe pas!!!");
        }

        $form = $this->data[$formName];// Objet Form

        include "views/forms/".$formTemplate.".view.php";
    }

    public function assets(string $type,string $file){
        return "/public/".$type."/".$file;
    }

    public function getUser(){
        if (session_status() === 'PHP_SESSION_ACTIVE'){
            return $this->um->findOneBy(['id'=>$_SESSION['user_id']]);
        }
        return null;
    }

    public function __destruct()
    {
        extract($this->data);
        include "views/layouts/".$this->template.".tpl.php" ;
    }
}