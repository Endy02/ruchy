<?php

namespace Ruchy\Core;

use Ruchy\Managers;


class Model implements \JsonSerializable
{

    public function __toArray(): array
    {
        return get_object_vars($this);
    }

    // Il est possible ici de remplacer l'objet courant par $this si vous le souhaitez
    public function hydrate(array $row)
    {
        $className = get_class($this);// $className = static::class
        $articleObj = new $className();
        foreach ($row as $key => $value) {

            $method = 'set'.ucFirst($key);
            if (method_exists($articleObj, $method)) {
                // Author = 4
                if($relation = $articleObj->getRelation($key)) {
                    // Relation = User::class (App\Model\User)
                    $tmp = new $relation();

                    $tmp = $tmp->hydrate($row);
                    // Maintenant on récupère notre id qui est ... la valeur actuelle de notre objet
                    $tmp->setId($value);
                    $articleObj->$method($value);
                } else {
                    $articleObj->$method($value);
                }
            }
        }
        return $articleObj;
    }

    public function jsonSerialize() {

        return $this->__toArray();
    }


    public function getRelation(string $key): ?string
    {
        $relations = $this->initRelation();

        if(isset($relations[$key]))
            return $this->initRelation()[$key];

        return null;
    }

}
