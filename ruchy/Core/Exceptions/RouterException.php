<?php


namespace Ruchy\Core\Exceptions;


use Ruchy\Core\View;
use Exception;

class RouterException extends Exception
{
    public function __construct($message, $code = 404) {
        new View("404", "base");
        //parent::__construct($message, $code);
        die();
    }
}