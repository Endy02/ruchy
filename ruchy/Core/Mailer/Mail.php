<?php


namespace Ruchy\Core\Mailer;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require $_SERVER['DOCUMENT_ROOT'].'/Includes/PHPMailer/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'].'/Includes/PHPMailer/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'].'/Includes/PHPMailer/src/SMTP.php';

class Mail
{
    private $host = 'smtp.gmail.com';
    private $SMTPUsername = 'fujyn.yokai@gmail.com';
    private $senderName = 'contact@ruchy.com';
    private $password = 'omsireodgdtxipdi';
    private $username;
    private $recipient;
    private $content;
    private $mail;
    private $subject;

    public function __construct($recipient, $content, $username, $subject){
        $this->recipient = $recipient;
        $this->content   = $content;
        $this->username  = $username;
        $this->subject   = $subject;
        $this->mail      = new PHPMailer(true);

        $this->mail->isSMTP();
        $this->mail->SMTPAuth   = true;
        $this->mail->SMTPDebug  = 0;
        $this->mail->Host       = $this->host;
        $this->mail->Username   = $this->SMTPUsername;
        $this->mail->Password   = $this->password;
        $this->mail->SMTPSecure = 'tls';
        $this->mail->Port       = 587;

        $this->mail->setFrom($this->SMTPUsername, $this->senderName);
        $this->mail->addAddress($this->recipient, $this->username);

        $this->mail->Subject = $this->subject;
        $this->mail->Body = $this->content;
    }

    // Send the mail
    public function sendMail(){
        try {
            if($this->mail->send()){
                return true;
            }
        } catch (Exception $e) {
            var_dump($this->mail->ErrorInfo);die();
        }
    }
}