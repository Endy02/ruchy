<?php

namespace Ruchy\Core;

use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Constraints\Validator;

class Form
{
    private $builder;
    private $config = [];
    private $model;
    private $name;
    private $action;
    private $isSubmit = false;
    private $isValid = false;
    private $validator;
    private $errors = [];

    //Initialise le validator et mets les valeurs par défaut dans la config
    public function __construct()
    {
        $this->validator = new Validator();

        $this->config = [
            "method"=>"POST",
            "attr" => [ ]
        ];
    }


    // QUELQUES METHODES

    //Parcours les élements du Builder en récupérant le nom (exemple:firstname)
    // Si le getter de ce nom existe dans le model lié à la page on le modifie
    public function associateValue()
    {

        foreach($this->builder->getElements() as $key => $element)
        {
            $method = 'get'.ucfirst($key);

            if(method_exists($this->model, $method))
            {
                $this->builder->setValue($key, $this->model->$method());
            }
        }

    }

    // Je l'utilise pour me simplifier la vie, il ne fait que récupèrer les élèments du builder
    public function getElements(): ?array
    {
        return $this->builder->getElements();
    }

    /**
     *  Si on est en POST, on update isSubmit en lançant checkIsSubmit
     *      si on est isSubmit on update isValid en lançant checkIsValid
     *      Quand on termine, on associe les valeurs des champs du formulaire à notre $model
     *
     * */
    public function handle(): void
    {
        if($_SERVER['REQUEST_METHOD'] === $this->config["method"])
        {
            $isSubmit = $this->checkIsSubmitted();
            if($isSubmit)
            {
                $this->checkIsValid();
            }

            $this->updateObject();
        }
    }

    /**
     * Comme on peut faire plusieurs formulaire dans une page, le name en front
     * contient le $nomFormulaire_$nomDuChamps, donc je parcours mes données en POST
     * et si une clé contient le $nomFormulaire alors je sais que c'est le bon formulaire qui est soumis
     * Le nom se trouve dans $name / Elle update isSubmit
     */
    private function checkIsSubmitted()
    {
        foreach($_POST as $key => $value)
        {
            if(FALSE !== strpos($key, $this->name))
            {
                $this->isSubmit = true;
                return true;
            }
        }

        return false;
    }

    /**
     * Cette méthode regarde pour chaque élements du builder l'ensemble des contraintes
     * Si il y a des contraintes, alors elles les enregistres dans $errors
     * Elle update $isValid
     */
    public function checkIsValid(): void
    {
        $this->isValid = true;

        foreach($_POST as $key => $value)
        {

            if(FALSE !== strpos($key, $this->name))
            {
                //testtype_firstname
                $key = str_replace($this->name.'_', '', $key);
                //firstname
                $element = $this->builder->getElement($key);

                if(isset($element->getOptions()['constraints']))
                {
                    foreach($element->getOptions()['constraints'] as $title => $constraint){
                        switch ($title){
                            case 'type':
                                $this->checkConstraint($constraint,$value,$key);
                                break;
                            case 'max-length':
                                $this->checkMaxLength($constraint,$value,$key);
                                break;
                            case 'min-length':
                                $this->checkMinLength($constraint,$value,$key);
                                break;
                        }
                    }
                }
            }
        }
    }

    public function checkConstraint($constraint,$value,$inputName){
        switch ($constraint){
            case 'input-name':
                if (!preg_match('/^[a-zA-Z]+$/',$value)){
                    $_SESSION['errors'][$inputName] = "Your {$inputName} may contain only caracteres";
                    $this->isValid = false;
                }
                break;
            case 'input-username':
                if (!preg_match('/^[a-zA-Z0-9]+$/',$value)){
                    $_SESSION['errors'][$inputName] = 'Your Username can only contain alfanumeric caracteres and numbers';
                    $this->isValid = false;
                }
                break;
            case 'input-email':
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)){
                    $_SESSION['errors'][$inputName] = 'Your Email is not valid';
                    $this->isValid = false;
                }
                break;
            case 'input-password':
                if(!preg_match('/^[0-9A-Za-z]{8,100}$/', $value)) {
                    $_SESSION['errors'][$inputName] = 'the password does not match with the requirements';
                    $this->isValid = false;
                }
                break;
            case 'input-file':
                $validExtension = ['jpeg','png','jpg','gif'];
                if(in_array($value)) {
                    $_SESSION['errors'][$inputName] = 'the file must be in ';
                    $this->isValid = false;
                }
                break;
        }
    }

    public function checkMaxLength($constraint,$value,$key){
        if(strlen($value) > $constraint){
            $_SESSION['errors'][$key] = "Your {$key} may have a max size of {$constraint} caracters";
            $this->isValid = false;
        }
    }

    public function checkMinLength($constraint,$value,$key){
        if(strlen($value) < $constraint){
            $_SESSION['errors'][$key] = "Your {$key} may have a min size of {$constraint} caracters";
            $this->isValid = false;
        }
    }

    public function regexCheck() : array
    {
        foreach($_POST as $key => $value)
        {

            if(FALSE !== strpos($key, $this->name))
            {
                //testtype_firstname
                $key = str_replace($this->name.'_', '', $key);
                //firstname
                $element = $this->builder->getElement($key);
                var_dump($element);die();

            }
        }
    }

    // Insere les valeurs du formulaire dans $model
    public function updateObject(): void
    {
        foreach($_POST as $key => $value)
        {
            if(FALSE !== strpos($key, $this->name))
            {
                $key = str_replace($this->name.'_', '', $key);

                $method = 'set'.ucfirst($key);

                if(method_exists($this->model, $method))
                {
                    if (!is_string($value))
                    {
                        $value = intval($value->getId());
                    }
                    $this->model->$method($value);
                }
            }
        }

        $this->associateValue();
    }
// SETTER AND GETTER
    public function isSubmit(): bool
    {
        return $this->isSubmit;
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function setModel(Model $model): self
    {
        $this->model = $model;
        return $this;
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function setBuilder(FormBuilder $formBuilder): self
    {
        $this->builder = $formBuilder;

        return $this;
    }

    public function addConfig(string $key, $newConfig): self
    {
        $this->config[$key] = $newConfig;

        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return Form
     */
    public function setAction($action):self
    {
        $this->action = $action;
        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

}
