<?php

namespace Ruchy\Core\Connection;

use Ruchy\Core\Model;
use PDO;
use Throwable;

class PDOResult implements ResultInterface
{

    protected \PDOStatement $statement;

    public function __construct(\PDOStatement $statement)
    {
        $this->statement = $statement;
    }


    public function getArrayResult(string $class = null): array
    {
        $result =  $this->statement->fetchAll(PDO::FETCH_ASSOC);

        if($class) {
            $results = [];
            foreach ($result as $key => $value) {
                array_push($results, (new $class())->hydrate($value));
            }
            return $results;
        }

        return $result;


    }

    public function getOneOrNullResult(string $class = null): ?Model
    {
        $result =  $this->statement->fetch();
        if ($result !== false){
            if($class)
                return (new $class())->hydrate($result);
        }
        return null;

    }

    public function getValueResult()
    {
        return $this->statement->fetchColumn();
    }

}
