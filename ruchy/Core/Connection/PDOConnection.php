<?php

namespace Ruchy\Core\Connection;

use PDO;
use PDOException;
use Throwable;

class PDOConnection implements BDDInterface
{

    protected $pdo;

    public function __construct(){
        $this->connect();
    }

    public function connect(){
        try {
            $this->pdo = new PDO(DB_DRIVER.":host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PWD);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Throwable $e) {
            die($e->getMessage());
        }
    }

    public function query(string $query, array $parameters = null){
        if ($parameters) {
            $queryPrepared = $this->pdo->prepare($query);

            try {
                $queryPrepared->execute($parameters);
            }catch(PDOException $e){
                die($e->getMessage());
            }

            return new PDOResult($queryPrepared);
        } else {
            $queryPrepared = $this->pdo->prepare($query);
            $queryPrepared->execute();

            return new PDOResult($queryPrepared);
        }
    }

    public function getLastId(){
        return $this->pdo->lastInsertId();
    }
}
