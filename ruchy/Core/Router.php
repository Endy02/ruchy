<?php
namespace Ruchy\Core;

use Ruchy\Core\Exceptions\RouterException;
use Ruchy\Controllers;

class Router {
    private $url;
    private $params =[];
    private $request;
    private $matches;
    private array $listOfRoutes = [];

    public function __construct($url = null){
        $this->url = $url;
        $this->listOfRoutes = yaml_parse_file('route.yml');
        $this->run();
    }

    public function run(){
        if($this->match($this->url)){
            $this->call();
        }else{
            throw new RouterException('No route matche');
        }
    }

    public function call(){
        if (isset($this->listOfRoutes[$this->request])){
            $controller = 'Ruchy\\Controllers\\'.ucfirst($this->listOfRoutes[$this->request]['controller']).'Controller';
            $method = $this->listOfRoutes[$this->request]['action'];
            if(class_exists($controller)){
                $controller = new $controller();
                if(method_exists($controller,$method)){
                    if (isset($this->matches)){
                        if (isset($this->matches[1])){
                            return $controller->$method($this->matches[0],$this->matches[1]);
                        }else{
                            return $controller->$method(implode($this->matches));
                        }
                    }
                    return $controller->$method();
                }
            }
        }else{
            throw new RouterException('No route Match');
        }
    }

    public function match($url){
        foreach (array_keys($this->listOfRoutes) as $stockUrl){
            $same = explode('/',$stockUrl);
            if (isset($same[2]) && isset(explode('/',trim($url))[2])){
                if(explode('/',trim($url,'/'))[0] == $same[1]){
                    $this->paramMatch($url,$stockUrl);
                    return true;
                }
            }
            if ($url == $stockUrl){
                $this->request = $stockUrl;
                return true;
            }
        }
        return false;
    }

    public function paramMatch($url,$request){
        $path = preg_replace('#:([\w]+)#','([^/]+)',$request);
        $regex = "#^$path$#i";
        if(!preg_match($regex,$url, $matches)){
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;
        $this->request = $request;
    }

    public function getRoute($name){
        $url = array_search($name, array_column($this->listOfRoutes,'action'));
        $allUrl = array_keys($this->listOfRoutes);
        var_dump($allUrl[$url]);
        return $allUrl[$url];
    }

}