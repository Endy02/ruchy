<?php

namespace Ruchy\Core\Builder;

class FormBuilder implements FormBuilderInterface
{
    private array $elements = [];


    public function add(string $name, string $type, array $options = []): FormBuilderInterface
    {
        $this->elements[$name] =
            (new ElementFormBuilder())
                ->setName($name)
                ->setType($type)
                ->setOptions($options);

        return $this;
    }

    public function remove(string $name): FormBuilderInterface
    {
        unset($this->elements[$name]);

        return $this;
    }

    public function getElements(): ?array
    {
        return $this->elements;
    }

    public function getElement(string $value): ?ElementFormBuilderInterface
    {
        return $this->elements[$value];
    }

    public function setValue(?string $key, $value): FormBuilderInterface
    {
        $this->elements[$key]->setValue($key, $value);

        return $this;
    }
}
