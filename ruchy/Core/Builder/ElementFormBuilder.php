<?php

namespace Ruchy\Core\Builder;

class ElementFormBuilder implements ElementFormBuilderInterface
{
    private string $name;

    private string $type;

    private array $options;

    public function __construct()
    {
        $this->options = [];
    }

    public function setValue(?string $key, $value): ElementFormBuilderInterface
    {
        if (is_array($value)){
            $this->options['value'] = $value->getId();
        }
        $this->options['value'] = $value;

        return $this;
    }

    public function setName(string $name): ElementFormBuilderInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setType(string $type): ElementFormBuilderInterface
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setOptions(array $options): ElementFormBuilderInterface
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

}
