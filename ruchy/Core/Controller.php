<?php

namespace Ruchy\Core;

use Ruchy\Core\Router;
use Ruchy\Managers\UserManager;
use Ruchy\Models\User;
use http\Header;
use SplObserver;
use SplObjectStorage;
use Ruchy\Core\Builder\FormBuilder;
use Ruchy\Core\Events\ControllerEvent;

class Controller implements \SplSubject
{

    protected SplObjectStorage $observers;
    protected ControllerEvent $event;
    protected UserManager $um;

    public function __construct(){
        $this->event = new ControllerEvent();
        $this->observers = new SplObjectStorage();
        $this->um = new UserManager();
        $this->attach($this->event);
    }

    public function createForm(string $class, Model &$model = null): Form{
        $form = new $class;
        $form->configureOptions();
        $form->buildForm(new FormBuilder());

        if($model){
            $form->setModel($model);
            $form->associateValue();
        }

        return $form;
    }

    // Permet la redirection
    public function redirectTo(string $name, $param=[]){

        $listOfRoutes = new Router();
        $url = $listOfRoutes->getRoute($name);
        var_dump($url);die();
        if ($url) {
            return header('Location:'.$url);
        } else {
            die("Aucune correspondance pour la route");
        }
    }

    public function createToken($length){
        $caracteres = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return substr(str_shuffle(str_repeat($caracteres,$length)), 0, $length);
    }

    public function refresh(){
        header("Location:".$_GET['url']);
    }

    // Récupère l'utilisateur connecté où retourne null
    public function getUser(){
        if (!empty($_SESSION['user_id'])){
            return $this->um->findOneBy(['id'=>$_SESSION['user_id']]);
        }
        return null;
    }

    public function checkSession(): ?bool
    {
        $state = true;

        if (!isset($_SESSION['user_id'])){
            $state = false;
        }

        return $state;
    }

    public function userSession(Model $user){
        switch (session_status()){
            case 2 :
                session_destroy();
                session_start();
                break;
            case 1 :
                session_start();
                break;
        }

        $_SESSION['user_id'] = $user->getId();
        $_SESSION['user_email'] = $user->getEmail();
        $_SESSION['username'] = $user->getUsername();
        $_SESSION['token'] = $user->getToken();
        $_SESSION['ppUrl'] = $user->getPpUrl();
    }

    public function render(string $template, string $view, array $params = null){
        $myView = new View($view, $template);

        if (isset($params)){
            foreach($params as $key => $param) {
                $myView->assign($key, $param);
            }
        }
    }

    public function create(string $view, array $params = null){

        $myView = new View($view);

        if (isset($params)){
            foreach($params as $key => $param) {
                $myView->assign($key, $param);
            }
        }
    }

    public function attach(SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    public function notify()
    {
        /** @var SplObserver $observer */
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
