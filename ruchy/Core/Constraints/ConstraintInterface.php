<?php

namespace Ruchy\Core\Constraints;

interface ConstraintInterface
{
    public function isValid(string $value): bool;
    public function getErrors(): array;
}