<?php

namespace Ruchy\Core;

interface ModelInterface

{
    public function getId(): ?int;

    public function initRelation(): array;
}
