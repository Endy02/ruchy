<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$_SESSION['errors'] = [];

use Ruchy\Autoloader;
use Ruchy\Core\ConstantLoader;
use Ruchy\Core\Router;

require './Autoloader.php';

Autoloader::register();

new ConstantLoader();
new Router($_SERVER['REQUEST_URI']);