<?php
namespace Ruchy\Managers;

use Ruchy\Core\Manager;
use Ruchy\Core\Validator;
use Ruchy\Models\User;

class UserManager extends Manager {

    protected Validator $validator;

    public function __construct()
    {
        parent::__construct(User::class, 'user');
        $this->validator = new Validator();
    }

}