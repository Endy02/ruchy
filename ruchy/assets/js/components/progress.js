window.addEventListener('load', () => {
    const progress = document.querySelector('.progress-wrapper')
    var elem = document.getElementById("login-bar");   
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= 100) {
            progress.classList.add('hidden');
        } else {
            width++; 
            elem.style.width = width + '%'; 
        }
    }
})